# Generated by Django 4.2.13 on 2024-06-16 02:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveuniverse', '0010_alter_eveindustryactivityduration_eve_type_and_more'),
        ('marketmanager', '0054_remove_marginconfig_structure_type_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marginconfig',
            name='destination_solar_system',
            field=models.ManyToManyField(blank=True, related_name='+', to='eveuniverse.evesolarsystem', verbose_name='Destination Solar System'),
        ),
        migrations.AlterField(
            model_name='marginconfig',
            name='destination_structure',
            field=models.ManyToManyField(blank=True, related_name='+', to='marketmanager.structure', verbose_name='Destination Structure'),
        ),
        migrations.AlterField(
            model_name='marginconfig',
            name='source_solar_system',
            field=models.ManyToManyField(blank=True, related_name='+', to='eveuniverse.evesolarsystem', verbose_name='Source Solar System'),
        ),
        migrations.AlterField(
            model_name='marginconfig',
            name='source_structure',
            field=models.ManyToManyField(blank=True, related_name='+', to='marketmanager.structure', verbose_name='Source Structure'),
        ),
    ]
