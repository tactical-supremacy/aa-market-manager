# Generated by Django 4.0.3 on 2022-03-19 06:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('marketmanager', '0023_alter_typestatistics_eve_region'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='typestatistics',
            name='new_eden',
        ),
    ]
