# Generated by Django 4.0.3 on 2022-03-19 12:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('marketmanager', '0025_statisticsconfig'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='publicconfig',
            options={'default_permissions': (), 'verbose_name': 'Public Market Configuration'},
        ),
        migrations.AlterModelOptions(
            name='statisticsconfig',
            options={'default_permissions': (), 'verbose_name': 'TypeStatistics Calculation Configuration'},
        ),
    ]
