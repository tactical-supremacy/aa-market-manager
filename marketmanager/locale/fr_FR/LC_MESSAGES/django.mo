��    6      �  I   |      �     �  
   �  	   �  
   �     �  	   �     �  	   �            B   !     d     m     v     ~     �     �     �     �     �     �     �     �     �               0     ?     N     ]     n     s          �     �     �     �     �     �     �     �     �            	   $     .     ;     Q     X     ]     d     u     �  �  �     Q	     j	     {	     �	     �	     �	     �	  
   �	     �	     �	  ^   �	     U
     \
     e
     m
     t
     �
     �
     �
     �
     �
  !   �
          %     *      J     k     �     �     �     �     �     �  #   �  +        <  	   A     K     S     k     {     �     �     �  	   �     �     �     �          	            	   :        *         %         .               "      !   +           	      2                     4   &   6                           (                        #      /   )   5           $   0      ,      3          '                   -   1       
                    Buy 5th Percentile Buy Median Buy Order Buy Orders Buy Weighted Average Cancelled Channels Character Corporation Debug Channels Disabled due to Failure, Check reason, adjust config and re-enable Duration EVE Type Expired Expires Failure Reason Fetch Regions Is Corporation Issued Jita Comparison % Last Result Volume Last Task Runtime Last Updated Location Managed App Reason Managed App-Identifier Managed By App Market Watches Minimum Volume Missing Quantity Name Order Range Order State Owned Character Orders Owner Corporation ID Price Quantity Region Sell 5th Percentile Sell Median Sell Orders Sell Weighted Average Solar System Station Structure Structure ID Structure Type Filter System Type Volume Volume Remaining Wallet Division quantity Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-08 10:48+0000
Last-Translator: Joel Falknau <ozirascal@gmail.com>, 2023
Language-Team: French (France) (https://app.transifex.com/alliance-auth/teams/107430/fr_FR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_FR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Acheter le 5e percentile Acheter Médiane Ordre d'achat Commandes d'achat Acheter une moyenne pondérée Annulé Chaînes Personnage Corporation Canaux de débogage Désactivé en raison d'un échec, vérifiez la raison, ajustez la configuration et réactivez Durée Type EVE Expiré Expire Raison de l'échec Extraire les régions Est une Corporation Envoyer Comparaison de Jita % Volume du dernier résultat Exécution de la dernière tâche Dernière mise à jour Lieu Raison de l'application gérée Identifiant d'application géré Géré par application Surveillance des marcher Volume minimum Quantité manquante Nom Porter des commandes État de la commande Commandes de personnages possédés Identifiant de la Corporation propriétaire Prix Quantité Région Vendre le 5e percentile Vendre Médiane Commandes de vente Moyenne pondérée des ventes Système solaire Station Structure ID de structure Filtre de type de structure Système Type Volume Volume restant Division des portefeuilles quantité 